// const FAKE_NEWS_BASE_URL = "https://fakenews.redhitmark.ddnsfree.com"
const FAKE_NEWS_BASE_URL = "http://localhost:7000"

setTimeout( () => {
    const xhr = new XMLHttpRequest();
    xhr.open("POST", FAKE_NEWS_BASE_URL+"/api/predict/", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.onreadystatechange = function () {

        if (this.readyState !== 4) return;

        if (this.status === 200) {
            const objResponse = JSON.parse(this.responseText);
            console.log(objResponse);

            const popup = createPopup(objResponse.prediction === "Fake", objResponse.language);

            document.body.appendChild(popup)
        }
    };
    xhr.send(JSON.stringify({
        siteUrl: window.location.href.toString(),
        siteHtml: document.querySelector('body').innerHTML
    }));
},  2000)


function createPopup(isFakeNews, language) {
    const popup = document.createElement("div");

    popup.style.maxWidth = '450px';
    popup.style.width = '100%';
    popup.style.height = '200px';
    popup.style.background = 'white';
    popup.style.position = 'fixed';
    popup.style.zIndex = '99999999';
    popup.style.top = '0';
    popup.style.right = '0';
    popup.style.margin = '16px';
    popup.style.padding = '16px';
    popup.style.borderRadius = '4px';
    popup.style.boxShadow = '1px 1px 6px rgb(0 0 0 / 30%)';
    popup.style.display = 'flex';
    popup.style.flexDirection = 'row';
    popup.style.alignItems = 'center';
    popup.style.fontFamily = 'Arial'
    popup.style.fontWeight = 'bold'
    popup.style.fontSize = '16px';
    popup.style.color = 'black';

    const imagePopup = document.createElement("img");
    imagePopup.style.width = "100%";
    imagePopup.style.maxWidth = "150px";
    if (isFakeNews) {
        imagePopup.src = FAKE_NEWS_BASE_URL + "/static/fake-news-icon.png";
    } else {
        imagePopup.src =  FAKE_NEWS_BASE_URL + "/static/true-news-icon.png";
    }

    const bodyPopup = document.createElement("div");
    bodyPopup.style.maxWidth = "270x";
    bodyPopup.style.width = "100%";
    bodyPopup.style.display = 'flex';
    bodyPopup.style.flexDirection = "column";
    bodyPopup.style.textAlign = "center";
    if (isFakeNews) {
        bodyPopup.innerText = getFakeNewsWarningMessage(language);
    } else {
        bodyPopup.innerText = getRealNewsMessage(language);
    }

    const closePopup = document.createElement("div");
    closePopup.style.maxWidth = "30px";
    closePopup.style.width = "100%";
    closePopup.style.height = "100%";
    closePopup.style.display = 'flex';
    closePopup.style.flexDirection = "column";

    const closeButton = document.createElement("div");
    closeButton.style.textAlign = "center";
    closeButton.innerText = "×";
    closeButton.addEventListener("click", ev => {
        popup.style.display = "none";
    })
    closePopup.appendChild(closeButton);

    popup.appendChild(imagePopup)
    popup.appendChild(bodyPopup)
    popup.appendChild(closePopup)

    return popup;
}

function getRealNewsMessage(language) {
    switch (language) {
        case '__label__it':
            return "Questo contenuto sembra essere attendibile!";
        case '__label__en':
            return "This content looks a Real News!";
    }
}

function getFakeNewsWarningMessage(language) {
    switch (language) {
        case '__label__it':
            return "Attenzione! Questo contenuto potrebbe essere una Fake News!";
        case '__label__en':
            return "Be aware! This content may be a Fake News!";
    }
}